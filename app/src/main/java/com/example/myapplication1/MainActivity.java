package com.example.myapplication1;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication1.database.MyDatabase;
import com.example.myapplication1.util.Const;

import java.util.ArrayList;
import java.util.HashMap;

import static android.widget.Toast.LENGTH_LONG;


public class MainActivity extends AppCompatActivity {

    EditText etFirstName, etLastName, etEmail, etNumber;
    Button btnSubmit;
    TextView tvDisplay;
    ImageView ivClose;
    ImageView ivBackground;

    RadioGroup rbGender;
    RadioButton rbMale;
    RadioButton rbFemale;

    CheckBox chbMusic;
    CheckBox chbSurfing;
    CheckBox chbCarom;


    boolean isValid() {
        boolean flag = true;

        //Name validation
        if (TextUtils.isEmpty(etFirstName.getText())) {
            etFirstName.setError(getString(R.string.lbl_enter_value_fn));
            flag = false;
        } else {
            String fName = etFirstName.getText().toString().trim();
            String fNamePattern = "[a-zA-Z]+\\.?";
            if (!(fName.matches(fNamePattern))) {
                etFirstName.setError("Enter valid Name");
                flag = false;
                etFirstName.requestFocus();
            }
        }
        if (TextUtils.isEmpty(etLastName.getText())) {
            etLastName.setError(getString(R.string.lbl_enter_value_ln));
            flag = false;
        } else {
            String lName = etLastName.getText().toString().trim();
            String lNamePattern = "[a-zA-Z]+\\.?";
            if (!(lName.matches(lNamePattern))) {
                etLastName.setError("Enter valid Name");
               flag =  false;
               etLastName.requestFocus();
            }
        }

        //Number validation
        if (TextUtils.isEmpty(etNumber.getText())) {
            etNumber.setError(getString(R.string.lbl_enter_value_no));
            flag = false;
        } else {
            String phoneNumber = etNumber.getText().toString();
            if (phoneNumber.length() < 10) {
                etNumber.setError("Enter valid 10 digit number");
                flag =  false;
                etNumber.requestFocus();
            }
        }

        //Email validation
        if (TextUtils.isEmpty(etEmail.getText())) {
            etEmail.setError(getString(R.string.lbl_enter_email_e));
            flag = false;
        } else {
            String email = etEmail.getText().toString().trim();
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+\\.+[a-z]+";
            if (!(email.matches(emailPattern))) {
                etEmail.setError("Enter valid email address");
                flag =  false;
                etEmail.requestFocus();
            }

            //Checkbox validation
            if (!(chbMusic.isChecked() || chbSurfing.isChecked() || chbCarom.isChecked())) {
                Toast.makeText(this, "Please select any one checkbox", Toast.LENGTH_LONG).show();
                flag = false;
            }
        }return true;
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("TextViewValue", tvDisplay.getText().toString());
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new MyDatabase(MainActivity.this).getReadableDatabase();

        etFirstName = findViewById(R.id.etActFirstName);
        etLastName = findViewById(R.id.etActLastName);
        etEmail = findViewById(R.id.etActEmail);
        etNumber = findViewById(R.id.etActNumber);

        ivClose = findViewById(R.id.ivActClose);
        btnSubmit = findViewById(R.id.btnActSubmit);

        tvDisplay = findViewById(R.id.tvActDisplay);
        ivBackground = findViewById(R.id.ivActBackground);

        rbGender = findViewById(R.id.rgActGender);
        rbMale = findViewById(R.id.rbActMale);
        rbFemale = findViewById(R.id.rbActFemale);

        chbMusic = findViewById(R.id.chbActMusic);
        chbSurfing = findViewById(R.id.chbActSurfing);
        chbCarom = findViewById(R.id.chbActCarom);

        etNumber.setText("+91");




        final ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
        etFirstName.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.toString().trim().length()==0){
                    ivClose.setEnabled(false);
                } else {
                    ivClose.setEnabled(true);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isValid()) {

                    etFirstName.requestFocus();
                    etLastName.requestFocus();
                    etNumber.requestFocus();
                    etEmail.requestFocus() ;

                    HashMap<String, Object> map = new HashMap<>();
                    map.put(Const.FIRST_NAME, etFirstName.getText().toString());
                    map.put(Const.LAST_NAME, etLastName.getText().toString());
                    map.put(Const.NUMBER, etNumber.getText().toString());
                    map.put(Const.EMAIL, etEmail.getText().toString());
                    map.put(Const.GENDER, rbMale.isChecked() ? "M" : "F");

                    etFirstName.requestFocus();

                    String hobbies = "";
                    if (chbMusic.isChecked()) {
                        hobbies += "," + chbMusic.getText().toString();

                    }
                    if (chbSurfing.isChecked()) {
                        hobbies += "," + chbSurfing.getText().toString();
                    }
                    if (chbCarom.isChecked()) {
                        hobbies += "," + chbCarom.getText().toString();
                    }
                    if (hobbies.length() > 0) {
                        hobbies.substring(1);
                    }
                    map.put(Const.HOBBY, hobbies);
                    userList.add(map);

                    hobbies.replaceAll(", $", "");


                    Intent intent = new Intent(new Intent(MainActivity.this, SecondActivity.class));
                    String concateTempString = etFirstName.getText().toString() + " " + etLastName.getText().toString();
                    intent.putExtra("finalValue", concateTempString);
                    intent.putExtra("UserList", userList);
                    etFirstName.setText("");
                    etLastName.setText("");
                    etNumber.setText("");
                    etEmail.setText("");
                    rbGender.clearCheck();

                    if(chbCarom.isChecked()){
                        chbCarom.toggle();
                    }
                    if(chbSurfing.isChecked()){
                        chbSurfing.toggle();
                    }
                    if(chbMusic.isChecked()){
                        chbMusic.toggle();
                    }
                    startActivity(intent);


                    getSupportActionBar().setTitle(R.string.lbl_main_activity);

                }
            }

            public boolean onCreateOptionsMenu(Menu menu) {
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.dashboard_menu, menu);
                return true;
            }
        });

        tvDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String abc = "some one has $192 for currency conversion @70";
                int intValueGetFromEditText;
                if (etNumber.getText().toString().length() > 0) {
                    try {
                        intValueGetFromEditText = Integer.parseInt(etNumber.getText().toString());
                    } catch (Exception e) {
                        intValueGetFromEditText = 0;
                    }
                }
            }
        });
        rbGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                if (i == R.id.rbActMale) {
                    chbMusic.setVisibility(View.VISIBLE);
                    chbSurfing.setVisibility(View.VISIBLE);
                    chbCarom.setVisibility(View.VISIBLE);

                } else if (i == R.id.rbActFemale) {
                    chbMusic.setVisibility(View.GONE);
                    chbSurfing.setVisibility(View.VISIBLE);
                    chbCarom.setVisibility(View.VISIBLE);
                }
            }
        });


        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/BalsamiqSans-Bold.ttf");

        btnSubmit.setTypeface(typeface);
        tvDisplay.setTypeface(typeface);
        etFirstName.setTypeface(typeface);
        etLastName.setTypeface(typeface);
        etNumber.setTypeface(typeface);
        etEmail.setTypeface(typeface);




        etFirstName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etFirstName.getRight() - etFirstName.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        etFirstName.setText("");
                        return true;
                    }
                }
                return false;
            }
        });

        etLastName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etLastName.getRight() - etLastName.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        etLastName.setText("");
                        return true;
                    }
                }
                return false;
            }
        });


        etNumber.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etNumber.getRight() - etNumber.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        etNumber.setText("");
                        return true;
                    }
                }
                return false;
            }
        });

        etEmail.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etEmail.getRight() - etEmail.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        etEmail.setText("");
                        return true;
                    }
                }
                return false;
            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }

        });

    }
}
