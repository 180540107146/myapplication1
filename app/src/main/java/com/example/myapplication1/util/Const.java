package com.example.myapplication1.util;

public class Const {

    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String NUMBER = "number";
    public static final String EMAIL = "email";
    public static final String GENDER = "gender";
    public static final String HOBBY = "hobby";
}
