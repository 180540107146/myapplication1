package com.example.myapplication1;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication1.adapter.UserListAdapter;
import com.example.myapplication1.util.Const;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class SecondActivity extends AppCompatActivity {

    boolean doubleBackToExitPressedOnce = false;

    TextView tvDisplay;
    ListView lvUsers;
    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
    UserListAdapter userListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Log.d("Info::7::", "Button Pressed");
        initViewReference();
        bindViewValue();
    }

    void initViewReference() {
        lvUsers = findViewById(R.id.lvActUserList);
        tvDisplay = findViewById(R.id.tvActDisplay);
    }

    void bindViewValue() {
        userList.addAll((Collection<? extends HashMap<String, Object>>) getIntent().getSerializableExtra("UserList"));
        userListAdapter = new UserListAdapter(this, userList);
        lvUsers.setAdapter(userListAdapter);


        lvUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long itemId) {
                Toast.makeText(SecondActivity.this, userList.get(position).get(Const.GENDER).toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
