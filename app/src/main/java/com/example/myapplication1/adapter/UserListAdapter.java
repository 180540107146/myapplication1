package com.example.myapplication1.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.example.myapplication1.R;
import com.example.myapplication1.util.Const;

import java.util.ArrayList;
import java.util.HashMap;

public class UserListAdapter extends BaseAdapter {

    Context context;
    ArrayList<HashMap<String, Object>> userList;


    public UserListAdapter(Context context, ArrayList<HashMap<String, Object>> userList) {
        this.context = context;
        this.userList = userList;
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View view1 = LayoutInflater.from(context).inflate(R.layout.view_row_user_list, null);
        TextView tvName = view1.findViewById(R.id.tvLstName);
        TextView tvEmail = view1.findViewById(R.id.tvLstEmail);
        TextView tvGender = view1.findViewById(R.id.tvLstGender);

        tvName.setText(userList.get(position).get(Const.FIRST_NAME) + " " + userList.get(position).get(Const.LAST_NAME));
        tvEmail.setText(String.valueOf(userList.get(position).get(Const.EMAIL)));
        tvGender.setText(String.valueOf(userList.get(position).get(Const.GENDER)));


        return view1;
    }
}
